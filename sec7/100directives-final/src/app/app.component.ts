import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // numbers = [1, 2, 3, 4, 5];
  oddNumbers = [1, 3, 5];
  evenNumbers = [2, 4];
  onlyOdd = false;
  value = 5;
  buttonText = "Only show odd numbers";

  OnInit(){
    console.log(this.onlyOdd);
  }

  toggleButtonText(){
    console.log(this.onlyOdd);
    this.buttonText = "Only show ";
    if (this.onlyOdd){
      this.buttonText += "even ";
    }
    else {
      this.buttonText += "odd ";
    }
    this.buttonText += "numbers";
    this.buttonText = "Only show " + (this.onlyOdd ? "even " : "odd ") + "numbers";
    console.log(this.buttonText);
  }
}
